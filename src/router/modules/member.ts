import { RouteRecordRaw } from 'vue-router';
import { Layout } from '@/router/constant';
import { UserOutlined } from '@vicons/antd';
import { renderIcon } from '@/utils/index';

/**
 * @param name 路由名称, 必须设置,且不能重名
 * @param meta 路由元信息（路由附带扩展信息）
 * @param redirect 重定向地址, 访问这个路由时,自定进行重定向
 * @param meta.disabled 禁用整个菜单
 * @param meta.title 菜单名称
 * @param meta.icon 菜单图标
 * @param meta.keepAlive 缓存该路由
 * @param meta.sort 排序越小越排前
 *
 * */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/member',
    name: 'member',
    redirect: '/member/member-list',
    component: Layout,
    meta: {
      title: '会员管理',
      icon: renderIcon(UserOutlined),
      permissions: ['member_list', 'member_info', 'member_grade', 'member_withdrawal'],
      sort: 2,
    },
    children: [
      {
        path: 'member-list',
        name: 'memberList',
        meta: {
          title: '会员列表',
          permissions: ['member_list'],
          //keepAlive: true,
        },
        component: () => import('@/views/member/memberList/memberList.vue'),
      },
      {
        path: 'member-info/:id?',
        name: 'memberInfo',
        meta: {
          title: '会员',
          hidden: true,
          activeMenu: 'memberList',
          //permissions: ['member_info'],
          //keepAlive: true,
        },
        component: () => import('@/views/member/memberList/info.vue'),
      },
      {
        path: 'member-income/:id?',
        name: 'memberIncome',
        meta: {
          title: '会员收益',
          hidden: true,
          activeMenu: 'memberList',
          //permissions: ['member_income'],
          //keepAlive: true,
        },
        component: () => import('@/views/member/activityIncome/index.vue'),
      },
      {
        path: 'member-sign',
        name: 'memberSign',
        meta: {
          title: '签名管理',
          permissions: ['member_sign'],
          //keepAlive: true,
        },
        component: () => import('@/views/member/memberSign/index.vue'),
      },
      {
        path: 'member-grade',
        name: 'memberGrade',
        meta: {
          title: '会员等级',
          permissions: ['member_grade'],
          //keepAlive: true,
        },
        component: () => import('@/views/member/memberGrade/memberGrade.vue'),
      },
      {
        path: 'member-withdrawal',
        name: 'memberWithdrawal',
        meta: {
          title: '提现管理',
          permissions: ['member_withdrawal'],
          //keepAlive: true,
        },
        component: () => import('@/views/member/memberWithdrawal/index.vue'),
      },
      {
        path: 'member-balance',
        name: 'memberBalance',
        meta: {
          title: '会员余额记录',
          permissions: ['member_balance'],
          //keepAlive: true,
        },
        component: () => import('@/views/member/memberBalanceDetail/index.vue'),
      },
    ],
  },
];

export default routes;
