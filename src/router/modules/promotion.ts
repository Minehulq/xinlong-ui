import { RouteRecordRaw } from 'vue-router';
import { Layout } from '@/router/constant';
import { GiftOutlined } from '@vicons/antd';
import { renderIcon } from '@/utils/index';

/**
 * @param name 路由名称, 必须设置,且不能重名
 * @param meta 路由元信息（路由附带扩展信息）
 * @param redirect 重定向地址, 访问这个路由时,自定进行重定向
 * @param meta.disabled 禁用整个菜单
 * @param meta.title 菜单名称
 * @param meta.icon 菜单图标
 * @param meta.keepAlive 缓存该路由
 * @param meta.sort 排序越小越排前
 *
 * */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/promotion',
    name: 'promotion',
    redirect: '/promotion/rush-purchase',
    component: Layout,
    meta: {
      title: '促销活动',
      icon: renderIcon(GiftOutlined),
      permissions: [
        'rush_purchase',
        'rush_purchase_order',
        'put_sale_order_list',
        'rush_purchase_cart',
      ],
      sort: 4,
    },
    children: [
      {
        path: 'rush-purchase',
        name: 'rushPurchase',
        meta: {
          title: '抢购活动',
          permissions: ['rush_purchase'],
          //keepAlive: true,
        },
        component: () => import('@/views/promotion/rushPurchase/rushPurchase.vue'),
      },
      {
        path: 'rush-purchase-detail/:activityId?/:id?',
        name: 'rushPurchaseDetail',
        meta: {
          title: '活动详情',
          activeMenu: 'rushPurchase',
          //permissions: ['rush_purchase'],
          hidden: true,
          //keepAlive: true,
        },
        component: () => import('@/views/promotion/rushPurchase/rushPurchaseDetail.vue'),
      },
      {
        path: 'rush-purchase-order',
        name: 'rushPurchaseOrder',
        meta: {
          title: '抢购订单',
          permissions: ['rush_purchase_order'],
          //keepAlive: true,
        },
        component: () => import('@/views/promotion/rushPurchase/rushPurchaseOrder.vue'),
      },
      {
        path: 'put-sale-order',
        name: 'putSaleOrder',
        meta: {
          title: '上架订单',
          permissions: ['put_sale_order_list'],
          //keepAlive: true,
        },
        component: () => import('@/views/promotion/putSaleOrder/index.vue'),
      },
      {
        path: 'rush-purcahse-cart',
        name: 'rushPurcahseCart',
        meta: {
          title: '购物车',
          permissions: ['rush_purchase_cart'],
          //keepAlive: true,
        },
        component: () => import('@/views/promotion/rushPurchaseCart/index.vue'),
      },
    ],
  },
];

export default routes;
