import { RouteRecordRaw } from 'vue-router';
import { Layout } from '@/router/constant';
import { ShoppingOutlined } from '@vicons/antd';
import { renderIcon } from '@/utils/index';

/**
 * @param name 路由名称, 必须设置,且不能重名
 * @param meta 路由元信息（路由附带扩展信息）
 * @param redirect 重定向地址, 访问这个路由时,自定进行重定向
 * @param meta.disabled 禁用整个菜单
 * @param meta.title 菜单名称
 * @param meta.icon 菜单图标
 * @param meta.keepAlive 缓存该路由
 * @param meta.sort 排序越小越排前
 *
 * */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/goods',
    name: 'goods',
    redirect: '/goods/goods-list',
    component: Layout,
    meta: {
      title: '商品管理',
      icon: renderIcon(ShoppingOutlined),
      permissions: ['goods_list', 'goods_classify', 'goods_info', 'goods_brand'],
      sort: 3,
    },
    children: [
      {
        path: 'goods-list',
        name: 'goodsList',
        meta: {
          title: '商品列表',
          permissions: ['goods_list'],
          //keepAlive: true,
        },
        component: () => import('@/views/goods/goodsList/goodsList.vue'),
      },
      {
        path: 'goods-create/:id?',
        name: 'goodsCreate',
        meta: {
          title: '商品编辑',
          hidden: true,
          activeMenu: 'goods-list',
          // permissions: ['goods_create'], // 可以加菜单权限来限定权限
          //keepAlive: true,
        },
        component: () => import('@/views/goods/goods/goodsCreate.vue'),
      },
      // {
      //   path: 'goods-info/:id?',
      //   name: 'goods-info',
      //   meta: {
      //     title: '商品',
      //     hidden: true,
      //     activeMenu: 'goods-list',
      //     permissions: ['goods_info'],
      //     //keepAlive: true,
      //   },
      //   component: () => import('@/views/goods/goodsList/info.vue'),
      // },
      {
        path: 'goods-classify',
        name: 'goodsClassify',
        meta: {
          title: '商品分类',
          permissions: ['goods_classify'],
          //keepAlive: true,
        },
        component: () => import('@/views/goods/goodsClassify/goodsClassify.vue'),
        //component: () => import('@/views/form/basicForm/index.vue'),
      },
      {
        path: 'goods-brand',
        name: 'goodsBrand',
        meta: {
          title: '商品品牌',
          permissions: ['goods_brand'],
          //keepAlive: true,
        },
        component: () => import('@/views/goods/goodsBrand/goodsBrand.vue'),
      },
    ],
  },
];

export default routes;
