import { RouteRecordRaw } from 'vue-router';
import { Layout } from '@/router/constant';
import { UserOutlined } from '@vicons/antd';
import { renderIcon } from '@/utils/index';

/**
 * @param name 路由名称, 必须设置,且不能重名
 * @param meta 路由元信息（路由附带扩展信息）
 * @param redirect 重定向地址, 访问这个路由时,自定进行重定向
 * @param meta.disabled 禁用整个菜单
 * @param meta.title 菜单名称
 * @param meta.icon 菜单图标
 * @param meta.keepAlive 缓存该路由
 * @param meta.sort 排序越小越排前
 *
 * */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/site',
    name: 'site',
    redirect: '/site/article-list',
    component: Layout,
    meta: {
      title: '站点管理',
      icon: renderIcon(UserOutlined),
      permissions: ['article_list', 'logistics', 'adv_list', 'adv_promotion'],
      sort: 4,
    },
    children: [
      {
        path: 'home-site',
        name: 'homeSite',
        meta: {
          title: '首页管理',
          permissions: ['home_site'],
        },
        component: () => import('@/views/site/homeSite/index.vue'),
      },
      {
        path: 'adv-list',
        name: 'advList',
        meta: {
          title: '广告管理',
          permissions: ['adv_list'],
        },
        component: () => import('@/views/site/adv/index.vue'),
      },
      {
        path: 'adv-promotion',
        name: 'advPromotion',
        meta: {
          title: '广告位管理',
          permissions: ['adv_promotion'],
        },
        component: () => import('@/views/site/advPromotion/index.vue'),
      },
      {
        path: 'article-create/:id?',
        name: 'articleCreate',
        meta: {
          title: '文章编辑',
          hidden: true,
          activeMenu: 'article-list',
          // permissions: ['goods_create'], // 可以加菜单权限来限定权限
          //keepAlive: true,
        },
        component: () => import('@/views/site/article/articleCreate.vue'),
      },
      {
        path: 'logistics',
        name: 'logistics',
        meta: {
          title: '物流管理',
          hidden: false,
          permissions: ['logistics'],
          // permissions: ['goods_create'], // 可以加菜单权限来限定权限
          //keepAlive: true,
        },
        component: () => import('@/views/site/logistics/logistics.vue'),
      },
      {
        path: 'article-list',
        name: 'articleList',
        meta: {
          title: '文章管理',
          permissions: ['article_list'],
          //keepAlive: true,
        },
        component: () => import('@/views/site/articleList/articleList.vue'),
      },
    ],
  },
];

export default routes;
