import { RouteRecordRaw } from 'vue-router';
import { Layout } from '@/router/constant';
import { InboxOutlined } from '@vicons/antd';
import { renderIcon } from '@/utils/index';

/**
 * @param name 路由名称, 必须设置,且不能重名
 * @param meta 路由元信息（路由附带扩展信息）
 * @param redirect 重定向地址, 访问这个路由时,自定进行重定向
 * @param meta.disabled 禁用整个菜单
 * @param meta.title 菜单名称
 * @param meta.icon 菜单图标
 * @param meta.keepAlive 缓存该路由
 * @param meta.sort 排序越小越排前
 *
 * */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/blind-box',
    name: 'blindBox',
    redirect: '/blind-box/blind-box',
    component: Layout,
    meta: {
      title: '盲盒管理',
      icon: renderIcon(InboxOutlined),
      permissions: ['blind_box_list', 'blind_box_order'],
      sort: 4,
    },
    children: [
      {
        path: 'blind-box-list',
        name: 'blindBoxList',
        meta: {
          title: '盲盒列表',
          permissions: ['blind_box_list'],
          //keepAlive: true,
        },
        component: () => import('@/views/blindBox/blindBoxList/index.vue'),
      },
      {
        path: 'blind-box-create/:id?',
        name: 'blindBoxCreate',
        meta: {
          title: '盲盒编辑',
          hidden: true,
          activeMenu: 'blind-box-list',
          // permissions: ['goods_create'], // 可以加菜单权限来限定权限
          //keepAlive: true,
        },
        component: () => import('@/views/blindBox/blindBox/blindBoxCreate.vue'),
      },
      {
        path: 'blind-box-order',
        name: 'blindBoxOrder',
        meta: {
          title: '盲盒订单',
          permissions: ['blind_box_order'],
          //keepAlive: true,
        },
        component: () => import('@/views/blindBox/bindBoxOrder/index.vue'),
      },
    ],
  },
];

export default routes;
