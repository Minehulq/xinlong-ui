import { defineStore } from 'pinia';
import { createStorage } from '@/utils/Storage';
import { store } from '@/store';
import {
  ACCESS_TOKEN,
  REFRESH_ACCESS_TOKEN,
  CURRENT_USER,
  IS_LOCKSCREEN,
} from '@/store/mutation-types';
import { ResultEnum } from '@/enums/httpEnum';

const Storage = createStorage({ storage: localStorage });
import { getAdminInfo, login, refreshToken } from '@/api/system/admin';
import { storage } from '@/utils/Storage';

export interface IUserState {
  token: string;
  refreshToken: string;
  username: string;
  nickName: string;
  welcome: string;
  avatar: string;
  permissions: any[];
  info: any;
}

export const useUserStore = defineStore({
  id: 'app-user',
  state: (): IUserState => ({
    token: Storage.get(ACCESS_TOKEN, ''),
    refreshToken: Storage.get(REFRESH_ACCESS_TOKEN, ''),
    username: '',
    nickName: '',
    welcome: '',
    avatar: '',
    permissions: [],
    info: Storage.get(CURRENT_USER, {}),
  }),
  getters: {
    getToken(): string {
      return this.token;
    },
    getRefreshToken(): string {
      return this.refreshToken;
    },
    getAvatar(): string {
      return this.avatar;
    },
    getPermissions(): [any][] {
      return this.permissions;
    },
    getAdminInfo(): object {
      return this.info;
    },
  },
  actions: {
    setToken(token: string) {
      this.token = token;
    },
    setRefreshToken(refreshToken: string) {
      this.refreshToken = refreshToken;
    },
    setAvatar(avatar: string) {
      this.avatar = avatar;
    },
    setPermissions(permissions) {
      this.permissions = permissions;
    },
    setUserInfo(info) {
      this.info = info;
      this.nickName = info.nickName;
    },
    // 登录
    async login(userInfo) {
      try {
        const response = await login(userInfo);
        const { result, code } = response;
        if (code === ResultEnum.SUCCESS) {
          const ex = 2 * 60 * 60; // 用户token过期时间 2小时
          //const ex = 15; // 用户token过期时间 2小时
          const refershEx = 7 * 24 * 60 * 60; // 刷新token过期时间 7天
          storage.set(ACCESS_TOKEN, result.token, ex);
          storage.set(REFRESH_ACCESS_TOKEN, result.refreshToken, refershEx);
          storage.set(IS_LOCKSCREEN, false);
          this.setToken(result.token);
          this.setRefreshToken(result.refreshToken);
          //this.setUserInfo(result);
        }
        return Promise.resolve(response);
      } catch (e) {
        return Promise.reject(e);
      }
    },

    // 获取用户信息
    GetInfo() {
      const that = this;
      return new Promise((resolve, reject) => {
        getAdminInfo()
          .then((res) => {
            const result = res;
            if (result.permissions && result.permissions.length) {
              const permissionsList = result.permissions;
              const ex = 7 * 24 * 60 * 60;
              storage.set(CURRENT_USER, result, ex);
              that.setPermissions(permissionsList);
              that.setUserInfo(result);
            } else {
              reject(new Error('getInfo: permissionsList must be a non-null array !'));
            }
            that.setAvatar(result.avatar);
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    RefreshToken() {
      const that = this;
      const params = {
        userName: that.getAdminInfo.username,
        refreshToken: that.getRefreshToken,
      };
      return new Promise((resolve, reject) => {
        refreshToken(params)
          .then((res) => {
            const result = res;
            const ex = 2 * 60 * 60; // 用户token过期时间 2小时
            //const ex = 15;
            storage.set(ACCESS_TOKEN, result, ex);
            that.setToken(result);
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    // 登出
    async logout() {
      this.setPermissions([]);
      this.setUserInfo('');
      storage.remove(ACCESS_TOKEN);
      storage.remove(CURRENT_USER);
      storage.remove(REFRESH_ACCESS_TOKEN);
      return Promise.resolve('');
    },
  },
});

// Need to be used outside the setup
export function useUserStoreWidthOut() {
  return useUserStore(store);
}
