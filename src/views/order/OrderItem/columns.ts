export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 100,
  },
  {
    title: '订单ID',
    key: 'orderId',
    width: 100,
  },
  {
    title: '订单编号',
    key: 'orderSn',
    width: 100,
  },
  {
    title: '商品ID',
    key: 'goodsId',
    width: 100,
  },
  {
    title: '商品名称',
    key: 'goodsName',
    width: 100,
  },
  {
    title: '商品主图',
    key: 'goodsImg',
    width: 100,
  },
  {
    title: '商品数量',
    key: 'goodsNum',
    width: 100,
  },
  {
    title: '商品原价',
    key: 'goodsOriginalPrice',
    width: 100,
  },
  {
    title: '商品成交价',
    key: 'goodsPrice',
    width: 100,
  },
  {
    title: '商品优惠金额',
    key: 'goodsDiscounPrice',
    width: 100,
  },
  {
    title: '优惠规则',
    key: 'discountRule',
    width: 100,
  },
  {
    title: '状态',
    key: 'status',
    width: 100,
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
  },
];
