export interface Order {
  id: number;
  memberId: number;
  createTime: number;
  discountPrice: number;
  discountRule: string;
  goodsNum: number;
  goodsPrice: number;
  memberName: string;
  orderPrice: number;
  orderSn: string;
  orderStatus: number;
  orderType: number;
  payStatus: string;
  shipAreaId: number;
  shipAreaName: string;
  shipFullAddress: string;
  shipMobile: string;
  shipName: string;
  shippingPrice: number;
  completeTime: number;
  paymentTime: number;
}
