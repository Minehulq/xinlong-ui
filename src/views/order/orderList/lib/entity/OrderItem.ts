export interface OrderItem {
  id: number;
  orderId: number; //订单id
  orderSn: string; //订单编号
  goodsId: number; //商品id
  goodsImg: string; // 商品图片
  goodsName: string; //商品名字
  goodsNum: number; // 商品数量
  goodsOriginalPrice: number; // 商品原价
  goodsPrice: number; //商品价格
  discountRule: string; // 折扣规则
  goodsDiscountPrice: number; //商品折扣价格
  status: number; //订单项状态
  createTime: number; //创建时间
}
