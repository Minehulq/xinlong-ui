import { formatToDateTime } from '@/utils/dateUtil';
import { getOrderStatus } from './lib/enum/OrderEnum';
export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 40,
  },
  {
    title: '订单编号',
    key: 'orderSn',
    width: 100,
  },
  {
    title: '订单类型',
    key: 'orderType',
    width: 80,
    render: (data) => {
      switch (data.orderType) {
        case 1:
          return '正常';
        case 2:
          return '盲盒';
      }
    },
  },
  // {
  //   title: '会员名称',
  //   key: 'memberName',
  //   width: 80,
  // },
  {
    title: '订单状态',
    key: 'orderStatus',
    width: 80,
    render: (row) => {
      return getOrderStatus(row.orderStatus);
    },
  },
  // {
  //   title: '付款状态',
  //   key: 'payStatus',
  //   width: 80,
  //   render: (data) => {
  //     if (Number(data.payStatus) === 1) {
  //       return '已付款';
  //     } else {
  //       return '未付款';
  //     }
  //   },
  // },
  // {
  //   title: '支付时间',
  //   key: 'paymentTime',
  //   width: 100,
  //   render: (data) => {
  //     try {
  //       const paymentTime = data.paymentTime * 1000;
  //       return formatToDateTime(paymentTime);
  //     } catch (error) {
  //       console.log(error);
  //       return data.paymentTime;
  //     }
  //   },
  // },
  {
    title: '金额',
    key: 'orderPrice',
    width: 100,
  },
  // {
  //   title: '商品总金额',
  //   key: 'goodsPrice',
  //   width: 100,
  // },
  // {
  //   title: '商品总数',
  //   key: 'goodsNum',
  //   width: 100,
  // },
  {
    title: '收货人姓名',
    key: 'shipName',
    width: 100,
  },
  {
    title: '收货人手机号',
    key: 'shipMobile',
    width: 120,
  },
  {
    title: '下单时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
  // {
  //   title: '完成时间',
  //   key: 'completeTime',
  //   width: 100,
  //   render: (data) => {
  //     try {
  //       const completeTime = data.completeTime * 1000;
  //       return formatToDateTime(completeTime);
  //     } catch (error) {
  //       console.log(error);
  //       return data.completeTime;
  //     }
  //   },
  // },
];
