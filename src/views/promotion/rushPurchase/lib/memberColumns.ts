export const memberColumns = [
  {
    type: 'selection',
    multiple: false,
  },
  {
    title: 'ID',
    key: 'id',
    width: 50,
  },
  // {
  //   title: '分类',
  //   key: 'classifyName',
  //   width: 70,
  // },
  // {
  //   title: '品牌',
  //   key: 'brandName',
  //   width: 70,
  // },
  // {
  //   title: '会员编号',
  //   key: 'memberNo',
  //   width: 100,
  // },
  {
    title: '用户名',
    key: 'memberName',
    width: 80,
  },
  // {
  //   title: '会员密码',
  //   key: 'password',
  //   width: 100,
  // },
  {
    title: '手机号',
    key: 'mobile',
    width: 80,
  },
  {
    title: '昵称',
    key: 'nickname',
    width: 80,
  },
];
