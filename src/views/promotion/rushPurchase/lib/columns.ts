import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NSwitch } from 'naive-ui';
import { updateStatus } from '@/api/promotion/rushPurchase';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 30,
  },
  {
    title: '活动ID',
    key: 'activityId',
    width: 80,
  },
  {
    title: '活动名称',
    key: 'activityName',
    width: 100,
  },
  {
    title: '开始时间',
    key: 'startTime',
    width: 100,
    render: (data) => {
      try {
        const startTime = data.startTime * 1000;
        return formatToDateTime(startTime);
      } catch (error) {
        console.log(error);
        return data.startTime;
      }
    },
  },
  {
    title: '结束时间',
    key: 'endTime',
    width: 100,
    render: (data) => {
      try {
        const endTime = data.endTime * 1000;
        return formatToDateTime(endTime);
      } catch (error) {
        console.log(error);
        return data.endTime;
      }
    },
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
  {
    title: '访问人数',
    key: 'visitNum',
    width: 80,
  },
  {
    title: '交易量',
    key: 'totalPrice',
    width: 80,
  },
  {
    title: '活动状态',
    key: 'status',
    width: 80,
    render: (data) => {
      return h(NSwitch, {
        //disabled: true,
        checkedValue: 0,
        uncheckedValue: 1,
        defaultValue: data.status,
        onUpdateValue: async (value) => {
          // console.log(data.id);
          //console.log('xxxxx', value);
          await updateStatus({ id: data.id, status: value });
        },
      });
    },
  },
];
