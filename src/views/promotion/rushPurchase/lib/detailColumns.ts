import { h } from 'vue';
import { formatToDateTime } from '@/utils/dateUtil';
import { NSwitch } from 'naive-ui';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 50,
  },
  {
    title: '场次',
    key: 'activityId',
    width: 80,
  },
  // {
  //   title: '商品id',
  //   key: 'goodsId',
  //   width: 100,
  // },
  {
    title: '商品名称',
    key: 'goodsName',
    width: 150,
  },
  // {
  //   title: '卖家会员id',
  //   key: 'sellerId',
  //   width: 100,
  // },
  {
    title: '状态',
    key: 'status',
    width: 80,
    className: 'status',
    render: (data) => {
      const status = data.status;
      let text;
      switch (status) {
        case 0:
          text = '待售';
          break;
        case 1:
          text = '待支付';
          break;
        case 2:
          text = '待确认收款';
          break;
        case 3:
          text = '待上架';
          break;
        case 4:
          text = '已上架';
          break;
        case 5:
          text = '已取消';
          break;
        default:
      }
      return text;
    },
  },
  {
    title: '卖家名称',
    key: 'sellerName',
    width: 80,
  },
  {
    title: '卖家手机',
    key: 'sellerMobile',
    width: 100,
  },
  {
    title: '售价',
    key: 'price',
    width: 80,
  },
  {
    title: '积分',
    key: 'points',
    width: 80,
  },
  // {
  //   title: '买家会员id',
  //   key: 'buyerId',
  //   width: 100,
  // },
  {
    title: '买家名称',
    key: 'buyerName',
    width: 80,
  },
  {
    title: '买家手机号',
    key: 'buyerMobile',
    width: 100,
  },
  // {
  //   title: '支付凭证',
  //   key: 'payProof',
  //   width: 100,
  //   render: (data) => {
  //     return h(NImage, {
  //       src: data.payProof,
  //       width: 50,
  //     });
  //   },
  // },
  // {
  //   title: '购买时间',
  //   key: 'buyTime',
  //   width: 80,
  //   render: (data) => {
  //     try {
  //       const buyTime = data.buyTime * 1000;
  //       return formatToDateTime(buyTime);
  //     } catch (error) {
  //       console.log(error);
  //       return data.buyTime;
  //     }
  //   },
  // },
];
