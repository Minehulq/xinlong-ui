export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 80,
  },
  {
    title: '订单号',
    key: 'orderSn',
    width: 200,
  },
  // {
  //   title: '活动场次id',
  //   key: 'rushPurchaseId',
  //   width: 100,
  // },
  // {
  //   title: '抢购活动明细id',
  //   key: 'rushPurchaseDetailId',
  //   width: 100,
  // },
  // {
  //   title: '上架后，下一场抢购活动明细id',
  //   key: 'nextRushPurchaseDetailId',
  //   width: 100,
  // },
  {
    title: '原价（购买价）',
    key: 'originalPrice',
    width: 100,
  },
  {
    title: '售价',
    key: 'newPrice',
    width: 100,
  },
  {
    title: '是否预警',
    key: 'warning',
    width: 100,
    render: (data) => {
      const price = Number(Number(data.originalPrice * 1.04).toFixed(0));
      if (price > Number(Number(data.newPrice).toFixed(0))) {
        return '是';
      } else {
        return '否';
      }
    },
  },
  {
    title: '服务费',
    key: 'servicePrice',
    width: 100,
  },
  {
    title: '第三方支付单号',
    key: 'paymentOrderNo',
    width: 200,
  },
  // {
  //   title: '上架会员id',
  //   key: 'memberId',
  //   width: 100,
  // },
  {
    title: '状态',
    key: 'status',
    width: 100,
    render: (data) => {
      if (data.status == 1) {
        return '已支付';
      } else {
        return '未支付';
      }
    },
  },
];
