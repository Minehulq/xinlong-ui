import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NImage } from 'naive-ui';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 80,
  },
  // {
  //   title: '抢购商品id',
  //   key: 'rushPurchaseDetailId',
  //   width: 100,
  // },
  {
    title: '商品名称',
    key: 'goodsName',
    width: 200,
  },
  {
    title: '商品图片',
    key: 'goodsImg',
    width: 60,
    render: (data) => {
      return h(NImage, {
        src: data.goodsImg,
        width: 50,
      });
    },
  },
  {
    title: '价格',
    key: 'price',
    width: 100,
  },
  {
    title: '会员名称',
    key: 'nickname',
    width: 100,
  },
  {
    title: '手机',
    key: 'mobile',
    width: 100,
  },
  {
    title: '创建时间戳',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
