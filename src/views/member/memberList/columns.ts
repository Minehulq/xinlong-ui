import { updateMemberStatus } from '@/api/member/member';
import { formatToDateTime } from '@/utils/dateUtil';
import { NSwitch } from 'naive-ui';
import { h } from 'vue';
export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 30,
  },
  // {
  //   title: '会员编号',
  //   key: 'memberNo',
  //   width: 60,
  // },
  // {
  //   title: '用户名',
  //   key: 'memberName',
  //   width: 60,
  // },
  // {
  //   title: '会员密码',
  //   key: 'password',
  //   width: 100,
  // },
  {
    title: '手机号',
    key: 'mobile',
    width: 80,
  },
  {
    title: '昵称',
    key: 'nickname',
    width: 80,
  },
  {
    title: '积分',
    key: 'points',
    width: 50,
  },
  {
    title: '余额',
    key: 'balance',
    width: 50,
  },
  // {
  //   title: '性别',
  //   key: 'sex',
  //   width: 50,
  //   render: (data) => {
  //     let result = '';
  //     switch (data.sex) {
  //       case 0:
  //         result = '女';
  //         break;
  //       case 1:
  //         result = '男';
  //         break;
  //       default:
  //         result = '女';
  //     }
  //     return result;
  //   },
  // },
  // {
  //   title: '头像地址',
  //   key: 'face',
  //   width: 100,
  // },
  {
    title: '邀请人',
    key: 'inviteMemberNickname',
    width: 60,
  },
  {
    title: '邀请人手机号',
    key: 'inviteMemberMobile',
    width: 80,
  },
  {
    title: '会员等级',
    key: 'gradeName',
    width: 80,
  },
  {
    title: '状态',
    key: 'status',
    width: 60,
    render: (data) => {
      return h(NSwitch, {
        //disabled: true,
        checkedValue: 0,
        uncheckedValue: 1,
        defaultValue: data.status,
        onUpdateValue: async (value) => {
          await updateMemberStatus({ id: data.id, status: value });
        },
      });
    },
  },
  {
    title: '注册时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
  // {
  //   title: '上次登录时间',
  //   key: 'lastLogin',
  //   width: 100,
  // },
];
