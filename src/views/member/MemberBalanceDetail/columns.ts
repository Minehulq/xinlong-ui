import { formatToDateTime } from '@/utils/dateUtil';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 100,
  },
  {
    title: '会员姓名',
    key: 'nickname',
    width: 100,
  },
  {
    title: '会员手机号',
    key: 'mobile',
    width: 100,
  },
  {
    title: '类型',
    key: 'type',
    width: 100,
    render: (data) => {
      if (data.type == 0) {
        return '减少';
      } else if (data.type == 1) {
        return '增加';
      } else {
        return data.type;
      }
    },
  },
  {
    title: '金额',
    key: 'price',
    width: 100,
  },
  {
    title: '备注',
    key: 'memo',
    width: 100,
  },
  {
    title: '操作时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
