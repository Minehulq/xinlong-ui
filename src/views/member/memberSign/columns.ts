import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NImage } from 'naive-ui';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 100,
  },
  {
    title: '姓名',
    key: 'nickname',
    width: 100,
  },
  {
    title: '手机号',
    key: 'mobile',
    width: 100,
  },
  {
    title: '签名图片',
    key: 'signImg',
    width: 100,
    render: (data) => {
      return h(NImage, {
        src: data.signImg,
        width: 100,
      });
    },
  },
  {
    title: '签名时间',
    key: 'signTime',
    width: 100,
    render: (data) => {
      try {
        const signTime = data.signTime * 1000;
        return formatToDateTime(signTime);
      } catch (error) {
        console.log(error);
        return data.signTime;
      }
    },
  },
];
