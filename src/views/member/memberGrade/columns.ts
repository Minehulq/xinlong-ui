export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 100,
  },
  {
    title: '等级名称',
    key: 'gradeName',
    width: 100,
  },
  {
    title: '等级标识',
    key: 'gradeId',
    width: 100,
  },
  {
    title: '状态',
    key: 'disable',
    width: 100,
    render: (data) => {
      let result = '';
      switch (data.disable) {
        case 0:
          result = '启用';
          break;
        case 1:
          result = '禁用';
          break;
        default:
          result = '启用';
      }
      return result;
    },
  },
  {
    title: '排序',
    key: 'sort',
    width: 100,
  },
];
