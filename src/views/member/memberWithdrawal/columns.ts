import { formatToDateTime } from '@/utils/dateUtil';

export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 50,
  },
  {
    title: '姓名',
    key: 'nickname',
    width: 100,
  },
  {
    title: '手机号',
    key: 'mobile',
    width: 100,
  },
  {
    title: '提现金额',
    key: 'price',
    width: 100,
  },
  {
    title: '状态',
    key: 'status',
    width: 100,
    className: 'status',
    render: (data) => {
      let result = '';
      switch (data.status) {
        case 0:
          result = '待审核';
          break;
        case 1:
          result = '审核通过';
          break;
        case 2:
          result = '驳回';
          break;
        case 3:
          result = '打款成功';
          break;
        case 4:
          result = '用户取消';
          break;
        default:
          result = '异常';
      }
      return result;
    },
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
  {
    title: '更新时间',
    key: 'updateTime',
    width: 100,
    render: (data) => {
      try {
        if (data.updateTime) {
          const updateTime = data.updateTime * 1000;
          return formatToDateTime(updateTime);
        }
      } catch (error) {
        console.log(error);
        return data.updateTime;
      }
    },
  },
];
