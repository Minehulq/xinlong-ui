import { formatToDateTime } from '@/utils/dateUtil';
export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 100,
  },
  // {
  //   title: '会员id',
  //   key: 'memberId',
  //   width: 100,
  // },
  {
    title: '买入价格',
    key: 'buyPrice',
    width: 100,
  },
  {
    title: '卖出价格',
    key: 'sellPrice',
    width: 100,
  },
  {
    title: '服务费',
    key: 'servicePrice',
    width: 100,
  },
  {
    title: '利润',
    key: 'profit',
    width: 100,
  },
  {
    title: ' 活动期数',
    key: 'activityId',
    width: 100,
  },
  {
    title: '创建时间戳',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
