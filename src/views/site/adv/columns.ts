import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NImage } from 'naive-ui';

export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 60,
  },
  {
    title: '广告名称',
    key: 'advName',
    width: 120,
  },
  {
    title: '广告图片地址',
    key: 'advImg',
    width: 140,
    render: (data) => {
      return h(NImage, {
        src: data.advImg,
        width: 50,
      });
    },
  },
  {
    title: '广告地址',
    key: 'url',
    width: 160,
  },
  {
    title: '所属广告位',
    key: 'promotionName',
    width: 100,
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
