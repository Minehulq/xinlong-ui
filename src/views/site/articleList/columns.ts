import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NSwitch } from 'naive-ui';
import { updateStatus } from '@/api/site/article';

export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 100,
  },
  // {
  //   title: '文章分类id',
  //   key: 'articleClassifyId',
  //   width: 100,
  // },
  {
    title: '文章标题',
    key: 'articleTitle',
    width: 100,
  },
  {
    title: '文章封面',
    key: 'cover',
    width: 100,
    render: (data) => {
      return h('img', { src: data.cover, width: '50' });
    },
  },
  // {
  //   title: '文章内容',
  //   key: 'articleContent',
  //   width: 100,
  // },
  {
    title: '状态',
    key: 'status',
    width: 100,
    render: (data) => {
      return h(NSwitch, {
        //disabled: true,
        checkedValue: 1,
        uncheckedValue: 0,
        defaultValue: data.status,
        onUpdateValue: (value) => {
          // console.log(data.id);
          //console.log('xxxxx', value);
          updateStatus({ id: data.id, status: value });
        },
      });
    },
  },
  // {
  //   title: '排序',
  //   key: 'sort',
  //   width: 100,
  // },
  {
    title: '更新时间',
    key: 'updateTime',
    width: 100,
    render: (data) => {
      try {
        const updateTime = data.updateTime * 1000;
        return formatToDateTime(updateTime);
      } catch (error) {
        console.log(error);
        return data.updateTime;
      }
    },
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
