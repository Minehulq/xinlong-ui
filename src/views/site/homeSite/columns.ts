import { formatToDateTime } from '@/utils/dateUtil';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 100,
  },
  {
    title: '位置名称',
    key: 'siteName',
    width: 100,
  },
  {
    title: '位置编码',
    key: 'siteCode',
    width: 100,
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
