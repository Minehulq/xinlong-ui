import { formatToDateTime } from '@/utils/dateUtil';

export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 100,
  },
  {
    title: '广告位名称',
    key: 'promotionName',
    width: 100,
  },
  {
    title: '编码',
    key: 'promotionCode',
    width: 100,
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
