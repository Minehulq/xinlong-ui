import { h } from 'vue';
import { NSwitch } from 'naive-ui';
import { updateStatus } from '@/api/site/logistics';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 100,
  },
  {
    title: '公司名称',
    key: 'logisticsName',
    width: 100,
  },
  {
    title: '快递100编码',
    key: 'code',
    width: 100,
  },
  {
    title: '状态',
    key: 'status',
    width: 60,
    render: (data) => {
      return h(NSwitch, {
        //disabled: true,
        checkedValue: 1,
        uncheckedValue: 0,
        defaultValue: data.status,
        onUpdateValue: async (value) => {
          await updateStatus({ id: data.id, status: value });
        },
      });
    },
  },
  {
    title: '排序',
    key: 'sort',
    width: 100,
  },
];
