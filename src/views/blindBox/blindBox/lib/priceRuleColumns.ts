import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NInput, NInputNumber } from 'naive-ui';
export const priceRuleColumns = [
  {
    title: 'ID',
    key: 'id',
    width: 35,
  },
  {
    title: '名称',
    key: 'name',
    width: 150,
    render(row) {
      return h(NInput, {
        value: row.name,
        onUpdateValue(v) {
          row.name = v;
        },
      });
    },
  },
  {
    title: '购买次数',
    key: 'buyNum',
    width: 100,
    render(row) {
      return h(NInputNumber, {
        value: row.buyNum,
        onUpdateValue(v) {
          row.buyNum = v;
        },
      });
    },
  },
  {
    title: '价格',
    key: 'price',
    width: 100,
    render(row) {
      return h(NInputNumber, {
        value: row.price,
        onUpdateValue(v) {
          row.price = v;
        },
      });
    },
  },
];
