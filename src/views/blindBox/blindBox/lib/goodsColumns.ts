import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NSelect, NImage, NInputNumber } from 'naive-ui';
export const columns = [
  {
    title: '商品ID',
    key: 'goodsId',
    width: 35,
  },
  {
    title: '所属标签',
    key: 'goodsTag',
    width: 100,
    ellipsis: false,
    render: (data) => {
      const options = [];
      data.goodsTags.forEach((item) => {
        const opt = { label: item.name, value: item.id };
        options.push(opt);
      });
      return h(NSelect, {
        options: options,
        // defaultValue: 'bronze',
        onUpdateValue: (value) => {
          data.tag = value;
        },
        value: data.tag,
      });
    },
  },
  {
    title: '缩略图',
    key: 'goodsImg',
    width: 50,
    render: (data) => {
      return h(NImage, {
        src: data.goodsImg,
        width: 50,
      });
    },
  },
  {
    title: '权重',
    key: 'odds',
    width: 70,
    render(row) {
      return h(NInputNumber, {
        precision: 0,
        value: row.odds,
        onUpdateValue(v) {
          row.odds = v;
        },
      });
    },
  },
  {
    title: '库存',
    key: 'stock',
    width: 70,
    render(row) {
      return h(NInputNumber, {
        value: row.stock,
        onUpdateValue(v) {
          row.stock = v;
        },
      });
    },
  },
  {
    title: '商品名称',
    key: 'goodsName',
    width: 200,
  },
  // {
  //   title: '商品编号',
  //   key: 'goodsSkuSn',
  //   width: 70,
  // },
  {
    title: '价格',
    key: 'goodsPrice',
    width: 70,
  },
];
