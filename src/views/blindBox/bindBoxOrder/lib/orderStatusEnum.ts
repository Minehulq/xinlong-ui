/**
 * @description: 订单状态
 */
export enum BlindOrderStatusEnum {
    CREATE = 0,
    PAID_OFF = 1,
    PICK_UP = 2,
    COMPLETE = 3,
  }
  
  /**
   * 获得订单状态中文
   * @param status 
   * @returns 
   */
  export function getBlindOrderStatus(status: number) {
    switch (status) {
      case BlindOrderStatusEnum.CREATE:
        return '待付款';
      case BlindOrderStatusEnum.PAID_OFF:
        return '已付款';
      case BlindOrderStatusEnum.PICK_UP:
        return '已提货';
      case BlindOrderStatusEnum.COMPLETE:
        return '已完成';
    }
  }
  