import { formatToDateTime } from '@/utils/dateUtil';
import { getBlindOrderStatus } from './orderStatusEnum';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 50,
  },
  {
    title: '订单编号',
    key: 'orderSn',
    width: 150,
  },
  {
    title: '盲盒名称',
    key: 'blindBoxName',
    width: 100,
  },
  // {
  //   title: '盲盒id',
  //   key: 'blindBoxId',
  //   width: 75,
  // },
  {
    title: '会员昵称',
    key: 'nickname',
    width: 100,
  },
  // {
  //   title: '会员id',
  //   key: 'memberId',
  //   width: 100,
  // },
  {
    title: '会员手机号',
    key: 'memberMobile',
    width: 150,
  },
  // {
  //   title: '抽奖次数',
  //   key: 'blindBoxNum',
  //   width: 100,
  // },
  {
    title: '价格',
    key: 'price',
    width: 100,
  },
  {
    title: '状态',
    key: 'status',
    width: 100,
    render: (data) => {
      try {
        // if (data.status === 0) {
        //   return '待支付';
        // } else if (data.status === 1) {
        //   return '已支付';
        // } else if (data.status === 2) {
        //   return '已提货';
        // } else if (data.status === 3) {
        //   return '已完成';
        // }
        return getBlindOrderStatus(data.status);
      } catch (error) {
        console.log(error);
      }
    },
  },
  // {
  //   title: '第三方支付单号',
  //   key: 'paymentOrderNo',
  //   width: 100,
  // },
  // {
  //   title: '支付时间',
  //   key: 'payTime',
  //   width: 100,
  // },
  {
    title: '下单时间',
    key: 'createTime',
    width: 150,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
