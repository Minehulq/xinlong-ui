import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NImage, NSwitch } from 'naive-ui';
import { updateBlindBoxStatus } from '@/api/blindBox/blindBox';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 100,
    hidden: true,
  },
  {
    title: '盲盒名称',
    key: 'blindBoxName',
    width: 100,
  },
  {
    title: '背景图',
    key: 'backImg',
    width: 100,
    render: (data) => {
      return h(NImage, {
        src: data.backImg,
        width: 50,
      });
    },
  },
  {
    title: '盲盒标题图',
    key: 'blindBoxImg',
    width: 100,
    render: (data) => {
      return h(NImage, {
        src: data.blindBoxImg,
        width: 50,
      });
    },
  },
  {
    title: '单价',
    key: 'price',
    width: 100,
  },
  // {
  //   title: '价格规则',
  //   key: 'priceRules',
  //   width: 100,
  // },
  // {
  //   title: '产出规则',
  //   key: 'produceRule',
  //   width: 100,
  // },
  {
    title: '上架状态',
    key: 'status',
    width: 100,
    render: (data) => {
      return h(NSwitch, {
        //disabled: true,
        checkedValue: 1,
        uncheckedValue: 0,
        defaultValue: data.status,
        onUpdateValue: async (value) => {
          // console.log(data.id);
          // console.log(value);
          await updateBlindBoxStatus({ id: data.id, status: value });
        },
      });
    },
  },
  {
    title: '创建时间',
    key: 'createTime',
    width: 100,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
