// import { h } from 'vue';
// import { NTag } from 'naive-ui';

export const columns = [
  {
    title: 'id',
    key: 'id',
  },
  {
    title: '角色名称',
    key: 'roleName',
  },
  {
    title: '角色编码',
    key: 'roleCode',
  },
  {
    title: '描述',
    key: 'description',
  },
  // {
  //   title: '是否默认角色',
  //   key: 'isDefault',
  //   render(row) {
  //     return h(
  //       NTag,
  //       {
  //         type: row.isDefault ? 'success' : 'error',
  //       },
  //       {
  //         default: () => (row.isDefault ? '是' : '否'),
  //       }
  //     );
  //   },
  //},
  // {
  //   title: '创建时间',
  //   key: 'create_date',
  // },
];
