export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 100,
  },
  {
    title: '用户名',
    key: 'username',
    width: 100,
  },
  // {
  //   title: '头像',
  //   key: 'avatar',
  //   width: 100,
  // },
  // {
  //   title: '密码',
  //   key: 'password',
  //   width: 100,
  // },
  // {
  //   title: '描述',
  //   key: 'description',
  //   width: 100,
  // },
  {
    title: '昵称',
    key: 'nickName',
    width: 100,
  },
  {
    title: '全名',
    key: 'fullName',
    width: 100,
  },
  {
    title: '手机号',
    key: 'mobile',
    width: 100,
  },
  {
    title: '邮箱',
    key: 'email',
    width: 100,
  },
  {
    title: '状态',
    key: 'status',
    width: 100,
    render: (data) => {
      //console.log(index, data.status);
      let result = '';
      switch (data.status) {
        case 0:
          result = '正常';
          break;
        case 1:
          result = '禁用';
          break;
        case 2:
          result = '删除';
          break;
        default:
          result = '正常';
      }
      return result;
    },
  },
];
