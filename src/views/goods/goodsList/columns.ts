import { formatToDateTime } from '@/utils/dateUtil';
import { h } from 'vue';
import { NSwitch, NImage } from 'naive-ui';
import { updateStatus } from '@/api/goods/goods';
export const columns = [
  {
    title: 'ID',
    key: 'id',
    width: 35,
  },
  {
    title: '分类',
    key: 'classifyName',
    width: 50,
  },
  {
    title: '商品类型',
    key: 'goodsType',
    width: 50,
    render: (row) => {
      if (row.goodsType === 0) {
        return '普通商品';
      } else if (row.goodsType === 1) {
        return '抢购商品';
      } else if (row.goodsType === 2) {
        return '盲盒商品';
      } else {
        return '未知';
      }
    },
  },
  {
    title: '缩略图',
    key: 'goodsImgs',
    width: 50,
    render: (data) => {
      return h(NImage, {
        src: data.goodsImgs[0],
        width: 50,
      });
    },
  },
  {
    title: '商品名称',
    key: 'goodsName',
    width: 200,
  },
  {
    title: '商品编号',
    key: 'goodsSkuSn',
    width: 70,
  },
  // {
  //   title: '商品简介',
  //   key: 'goodsDesc',
  //   width: 100,
  // },
  {
    title: '价格',
    key: 'goodsPrice',
    width: 70,
  },
  // {
  //   title: '商品成本价格',
  //   key: 'goodsCostPrice',
  //   width: 100,
  // },
  // {
  //   title: '商品成本价格',
  //   key: 'goodsMarketPrice',
  //   width: 100,
  // },
  // {
  //   title: '商品货号',
  //   key: 'goodsSkuSn',
  //   width: 100,
  // },
  {
    title: '上架',
    key: 'goodsStatus',
    width: 60,
    render: (data) => {
      return h(NSwitch, {
        //disabled: true,
        checkedValue: 1,
        uncheckedValue: 0,
        defaultValue: data.goodsStatus,
        onUpdateValue: async (value) => {
          // console.log(data.id);
          // console.log(value);
          await updateStatus({ id: data.id, status: value });
        },
      });
    },
  },
  // {
  //   title: '商品详情',
  //   key: 'goodsIntro',
  //   width: 100,
  // },
  {
    title: '创建时间',
    key: 'createTime',
    width: 150,
    render: (data) => {
      try {
        const createTime = data.createTime * 1000;
        return formatToDateTime(createTime);
      } catch (error) {
        console.log(error);
        return data.createTime;
      }
    },
  },
];
