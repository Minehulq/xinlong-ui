import { h } from 'vue';
export const columns = [
  {
    title: 'id',
    key: 'id',
    width: 100,
  },
  {
    title: '品牌名称',
    key: 'brandName',
    width: 100,
  },
  {
    title: '图片地址',
    key: 'brandImg',
    width: 100,
    render: (data) => {
      return h('img', { src: data.brandImg, width: '50' });
    },
  },
  {
    title: '排序',
    key: 'sort',
    width: 100,
  },
];
