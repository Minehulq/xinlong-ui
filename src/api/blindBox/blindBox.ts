import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 盲盒分页
 */
export function getBlindBoxPage(params?) {
  return http.request({
    url: '/blind-box/blind-box/page',
    method: 'GET',
    params,
  });
}
/**
 * @description: 盲盒列表
 */
export function getBlindBoxList(params?) {
  return http.request({
    url: '/blind-box/blind-box/list',
    method: 'GET',
    params,
  });
}

/**
 * 查询盲盒信息
 * @param params
 */
export function getBlindBox(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box/${params.id}`,
      method: 'GET',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存盲盒
 * @param params
 */
export function saveBlindBox(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑盲盒
 * @param params
 */
export function updateBlindBox(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除盲盒
 * @param params
 */
export function deleteBlindBox(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑盲盒状态
 * @param params
 */
export function updateBlindBoxStatus(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box/status`,
      method: 'PUT',
      data: params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
