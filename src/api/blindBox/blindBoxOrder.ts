import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 盲盒订单表列表
 */
export function getBlindBoxOrderPage(params?) {
  return http.request({
    url: '/blind-box/blind-box-order/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存盲盒订单表
 * @param params
 */
export function saveBlindBoxOrder(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box-order`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑盲盒订单表
 * @param params
 */
export function updateBlindBoxOrder(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box-order/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除盲盒订单表
 * @param params
 */
export function deleteBlindBoxOrder(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/blind-box/blind-box-order/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
/***
 * 获取盲盒订单项
 * @param params
 */
export function getBlindOrderItem(params?) {
  return http.request<BasicResponseModel>({
    url: `/blind-box/blind-box-order/item/list/${params.id}`,
    method: 'GET',
    // params,
  });
};
