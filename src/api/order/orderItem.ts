import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 订单子项表列表
 */
export function getOrderItemPage(params?) {
  return http.request({
    url: '/order/orderItem/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存订单子项表
 * @param params
 */
export function saveOrderItem(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/order/orderItem`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑订单子项表
 * @param params
 */
export function updateOrderItem(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/order/orderItem/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除订单子项表
 * @param params
 */
export function deleteOrderItem(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/order/orderItem/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
