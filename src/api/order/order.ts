import { http } from '@/utils/http/axios';
import qs from 'qs';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 订单表列表
 */
export function getOrderPage(params?) {
  return http.request({
    url: '/order/order/page',
    method: 'GET',
    params,
    // 加这个是解决 传递数组时 会在参数名后面加上[]
    paramsSerializer: function (params) {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}
export function getOrderItemList(params?) {
  return http.request<BasicResponseModel>({
    url: `/order/order/item/list`,
    method: 'GET',
    params,
  });
}
/**
 * 获取物流公司列表
 * @param params
 */
export function getLogisticsList(params?) {
  return http.request({
    url: '/order/order/logistics',
    method: 'GET',
    params,
  });
}
/**
 * 订单发货
 * @param params
 */
export function orderShipment(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/order/order/shipment`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
