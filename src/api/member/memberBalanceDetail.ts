import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 用户余额明细列表
 */
export function getMemberBalanceDetailPage(params?) {
  return http.request({
    url: '/member/balance-detail/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存用户余额明细
 * @param params
 */
export function saveMemberBalanceDetail(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/balance-detail`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑用户余额明细
 * @param params
 */
export function updateMemberBalanceDetail(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/balance-detail/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除用户余额明细
 * @param params
 */
export function deleteMemberBalanceDetail(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/balance-detail/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
