import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 会员列表
 */
export function getMemberPage(params?) {
  return http.request({
    url: '/member/member/page',
    method: 'GET',
    params,
  });
}

/**
 * 保存会员
 * @param params
 */
export function saveMember(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑会员
 * @param params
 */
export function updateMember(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除会员
 * @param params
 */
export function deleteMember(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 获取会员默认地址
 */
export function getMemberDefAddress(params?) {
  return http.request({
    url: `/member/member/def-address/${params.memberId}`,
    method: 'GET',
    params,
  });
}

/**
 * @description: 修改会员状态
 */
export function updateMemberStatus(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/status`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}

/**
 * @description: 修改会员余额
 */
export function updateMemberBalance(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/balance/${params.memberId}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
