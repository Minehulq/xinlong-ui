import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 会员签名记录表列表
 */
export function getMemberSignPage(params?) {
  return http.request({
    url: '/member/member-sign/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存会员签名记录表
 * @param params
 */
export function saveMemberSign(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member-sign`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑会员签名记录表
 * @param params
 */
export function updateMemberSign(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member-sign/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除会员签名记录表
 * @param params
 */
export function passMemberSign(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member-sign/pass/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除会员签名记录表
 * @param params
 */
export function deleteMemberSign(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member-sign/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
