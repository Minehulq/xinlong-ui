import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 会员收益表列表
 */
export function getActivityIncomePage(params?) {
  return http.request({
    url: '/member/activity-income/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存会员收益表
 * @param params
 */
export function saveActivityIncome(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/activity-income`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑会员收益表
 * @param params
 */
export function updateActivityIncome(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/activity-income/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除会员收益表
 * @param params
 */
export function deleteActivityIncome(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/activity-income/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 获取会员收益信息
 */
export function getActivityIncomeSummary(params?) {
  return http.request({
    url: '/member/activity-income/income-summary',
    method: 'GET',
    params,
  });
}
