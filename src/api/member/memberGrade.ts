import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 会员等级分页列表
 */
export function getMemberGradePage(params?) {
  return http.request({
    url: '/member/member/grade/page',
    method: 'GET',
    params,
  });
}

/**
 * @description: 会员等级列表
 */
export function getMemberGradeList(params?) {
  return http.request({
    url: '/member/member/grade/list',
    method: 'GET',
    params,
  });
}

/**
 * 保存会员等级
 * @param params
 */
export function saveMemberGrade(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/grade`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑会员等级
 * @param params
 */
export function updateMemberGrade(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/grade/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除会员等级
 * @param params
 */
export function deleteMemberGrade(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/grade/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
