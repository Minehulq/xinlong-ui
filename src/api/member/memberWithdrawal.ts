import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 用户提现记录列表
 */
export function getMemberWithdrawalPage(params?) {
  return http.request({
    url: '/member/member/withdrawal/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存用户提现记录
 * @param params
 */
export function saveMemberWithdrawal(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/withdrawal`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑用户提现记录
 * @param params
 */
export function updateMemberWithdrawal(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/withdrawal/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除用户提现记录
 * @param params
 */
export function deleteMemberWithdrawal(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/withdrawal/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 审核通过
 * @param params
 */
export function passMemberWithdrawal(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/withdrawal/pass/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 审核拒绝
 * @param params
 */
export function refuseMemberWithdrawal(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/withdrawal/refuse/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 确认打款
 * @param params
 */
export function payMemberWithdrawal(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/member/member/withdrawal/pay/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
