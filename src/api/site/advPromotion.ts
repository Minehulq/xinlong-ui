import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 广告位列表
 */
export function getAdvPromotionPage(params?) {
  return http.request({
    url: '/site/promotion/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存广告位
 * @param params
 */
export function saveAdvPromotion(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/promotion`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑广告位
 * @param params
 */
export function updateAdvPromotion(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/promotion/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除广告位
 * @param params
 */
export function deleteAdvPromotion(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/promotion/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
