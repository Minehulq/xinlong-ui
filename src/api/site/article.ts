import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 文章列表列表
 */
export function getArticlePage(params?) {
  return http.request({
    url: '/site/article/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存文章列表
 * @param params
 */
export function saveArticle(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/article`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑文章列表
 * @param params
 */
export function updateArticle(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/article/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除文章列表
 * @param params
 */
export function deleteArticle(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/article/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 获取文章
 * @param params
 */
export function getArticle(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/article/${params.id}`,
      method: 'GET',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑文章状态
 * @param params
 */
export function updateStatus(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/article/status`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
