import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 广告表列表
 */
export function getAdvPage(params?) {
  return http.request({
    url: '/site/adv/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存广告表
 * @param params
 */
export function saveAdv(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/adv`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑广告表
 * @param params
 */
export function updateAdv(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/adv/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除广告表
 * @param params
 */
export function deleteAdv(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/adv/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 广告位类别
 */
export function getPromotionList(params?) {
  return http.request({
    url: '/site/promotion/list',
    method: 'GET',
    params,
  });
}

