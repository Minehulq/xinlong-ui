import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 物流公司列表
 */
export function getLogisticsPage(params?) {
  return http.request({
    url: '/site/logistics/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存物流公司
 * @param params
 */
export function saveLogistics(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/logistics`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑物流公司
 * @param params
 */
export function updateLogistics(params?) {
  console.log('params',params)
  return http.request<BasicResponseModel>(
    {
      url: `/site/logistics/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除物流公司
 * @param params
 */
export function deleteLogistics(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/logistics/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 *  * 状态更新物流公司
 * @param params
 * 
 */
export function updateStatus(params?) {
  console.log(params)
  return http.request<BasicResponseModel>(
    {
      url: `/site/logistics/status`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}