import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 首页位置列表
 */
export function getHomeSitePage(params?) {
  return http.request({
    url: '/site/home-site/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存首页位置
 * @param params
 */
export function saveHomeSite(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/home-site`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑首页位置
 * @param params
 */
export function updateHomeSite(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/home-site/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除首页位置
 * @param params
 */
export function deleteHomeSite(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/site/home-site/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 首页位置选择的盲盒商品列表
 */
export function getSelectBlindBox(params?) {
  return http.request({
    url: '/site/home-site/select',
    method: 'GET',
    params,
  });
}

/**
 * @description: 保存首页位置选择的盲盒商品列表
 */
export function saveSelectBlindBox(params?) {
  return http.request(
    {
      url: '/site/home-site/select',
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
