import { http } from '@/utils/http/axios';
import qs from 'qs';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 抢购订单活动明细表列表
 */
export function getRushPurchaseOrderPage(params?) {
  return http.request({
    url: '/promotion/rush-purchase/order/page',
    method: 'GET',
    params,
    // 加这个是解决 传递数组时 会在参数名后面加上[]
    paramsSerializer: function (params) {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}

/**
 * 取消订单
 * @param params
 */
export function cancelOrder(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/order/cancel`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}

/**
 * 订单确认收款
 * @param params
 */
export function orderConfirmReceipt(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/order/confirm-receipt`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
/**
 * 获取订单统计
 * @param params
 * @returns
 */
export function getRushPurchaseOrderSummary(params?) {
  return http.request({
    url: '/promotion/rush-purchase/order/order-summary',
    method: 'GET',
    params,
  });
}
/**
 * 获得订单上架信息
 * @param params
 * @returns
 */
export function getPutSaleInfo(params?) {
  return http.request({
    url: '/promotion/rush-purchase/order/put-sale-info',
    method: 'GET',
    params,
  });
}
/**
 * 订单上架，会创建上架单，然后自动上架
 * @param params
 * @returns
 */
export function putSale(params?) {
  return http.request(
    {
      url: '/promotion/rush-purchase/order/put-sale',
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
