import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 抢购活动表列表
 */
export function getRushPurchasePage(params?) {
  return http.request({
    url: '/promotion/rush-purchase/page',
    method: 'GET',
    params,
  });
}

/**
 * 保存抢购活动表
 * @param params
 */
export function saveRushPurchase(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑抢购活动表
 * @param params
 */
export function updateRushPurchase(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除抢购活动表
 * @param params
 */
export function deleteRushPurchase(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑抢购活动状态
 * @param params
 */
export function updateStatus(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/status`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
/**
 * 编辑抢购活动是否是vip专区
 * @param params
 */
export function updateIsVip(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/vip/${params.id}/${params.isVip}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
