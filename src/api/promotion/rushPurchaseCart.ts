import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 抢购购物车列表
 */
export function getRushPurchaseCartPage(params?) {
  return http.request({
    url: '/promotion/rush-purchase/cart/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存抢购购物车
 * @param params
 */
export function saveRushPurchaseCart(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/cart`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑抢购购物车
 * @param params
 */
export function updateRushPurchaseCart(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/cart/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除抢购购物车
 * @param params
 */
export function deleteRushPurchaseCart(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/cart/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
