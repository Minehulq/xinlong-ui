import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 抢购活动明细表列表
 */
export function getRushPurchaseDetailPage(params?) {
  return http.request({
    url: '/promotion/rush-purchase/detail/page',
    method: 'GET',
    params,
  });
}

/**
 * 批量保存抢购活动明细表
 * @param params
 */
export function saveRushPurchaseDetails(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/detail/batch-save`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑抢购活动明细表
 * @param params
 */
export function updateRushPurchaseDetail(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/detail/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除抢购活动明细表
 * @param params
 */
export function deleteRushPurchaseDetail(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/rush-purchase/detail/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 获取所有商品列表，后期换成分页的
 * @description: 商品列表
 */
export function getGoodsList(params?) {
  return http.request({
    url: '/promotion/rush-purchase/detail/goods-list',
    method: 'GET',
    params,
  });
}

/**
 * 获取所有会员列表，后期换成分页的
 * @description: 商品列表
 */
export function getMembersList(params?) {
  return http.request({
    url: '/promotion/rush-purchase/detail/member-list',
    method: 'GET',
    params,
  });
}
