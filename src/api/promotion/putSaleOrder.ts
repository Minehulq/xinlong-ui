import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 上架订单列表列表
 */
export function getPutSaleOrderPage(params?) {
  return http.request({
    url: '/promotion/put-sale-order/page',
    method: 'GET',
    params,
  });
}
//isTransformResponse true不进行任何处理直接返回， false会进行处理后返回 例如权限不够都会处理
/**
 * 保存上架订单列表
 * @param params
 */
export function savePutSaleOrder(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/put-sale-order`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 人工上架
 * @param params
 */
export function putSaleOrder(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/promotion/put-sale-order/put-sale`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}

/**
 * 编辑上架订单列表
 * @param params
 */
// export function updatePutSaleOrder(params?) {
//   return http.request<BasicResponseModel>(
//     {
//       url: `/promotion/put-sale-order/${params.id}`,
//       method: 'PUT',
//       params,
//     },
//     {
//       isTransformResponse: false,
//     }
//   );
// }

// /**
//  * 删除上架订单列表
//  * @param params
//  */
// export function deletePutSaleOrder(params?) {
//   return http.request<BasicResponseModel>(
//     {
//       url: `/promotion/put-sale-order/${params.id}`,
//       method: 'DELETE',
//       params,
//     },
//     {
//       isTransformResponse: false,
//     }
//   );
// }
