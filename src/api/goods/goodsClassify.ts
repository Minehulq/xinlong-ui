import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 列表
 */
export function getGoodsClassifyList(params?) {
  return http.request({
    url: '/goods/classify/list',
    method: 'GET',
    params,
  });
}

/**
 * 保存
 * @param params
 */
export function saveGoodsClassify(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/classify`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑
 * @param params
 */
export function updateGoodsClassify(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/classify/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除
 * @param params
 */
export function deleteGoodsClassify(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/classify/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 修改分类显示状态
 * @param params
 */
export function updateGoodsClassifyShow(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/classify/show`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}
