import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 商品列表
 */
export function getGoodsPage(params?) {
  return http.request({
    url: '/goods/goods/page',
    method: 'GET',
    params,
  });
}

/**
 * 保存商品
 * @param params
 */
export function saveGoods(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/goods`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 获取商品
 * @param params
 */
export function getGoods(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/goods/${params.id}`,
      method: 'GET',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑商品
 * @param params
 */
export function updateGoods(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/goods/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除商品
 * @param params
 */
export function deleteGoods(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/goods/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑商品状态
 * @param params
 */
export function updateStatus(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/goods/status`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
      joinParamsToUrl: true,
    }
  );
}

/**
 * 获取所有商品
 * @param params
 * @returns
 */
export function getAllGoods(params?) {
  return http.request({
    url: '/goods/goods/all',
    method: 'GET',
    params,
  });
}
