import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 商品品牌列表
 */
export function getBrandPage(params?) {
  return http.request({
    url: '/goods/brand/page',
    method: 'GET',
    params,
  });
}

/**
 * 保存商品品牌
 * @param params
 */
export function saveBrand(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/brand`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑商品品牌
 * @param params
 */
export function updateBrand(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/brand/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除商品品牌
 * @param params
 */
export function deleteBrand(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/goods/brand/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 商品品牌列表
 */
export function getBrandList() {
  return http.request({
    url: '/goods/brand/list',
    method: 'GET',
  });
}
