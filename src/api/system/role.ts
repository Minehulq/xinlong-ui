import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 角色列表
 */
export function getRolePage(params?) {
  return http.request({
    url: '/sys/role/page',
    method: 'GET',
    params,
  });
}

/**
 * 保存角色菜单权限
 * @param params
 */
export function savePermissions(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/sys/role/save-permissions`,
      method: 'GET',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 保存角色
 * @param params
 */
export function saveRole(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/sys/role`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑角色
 * @param params
 */
export function updateRole(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/sys/role/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除角色
 * @param params
 */
export function deleteRole(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/sys/role/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
