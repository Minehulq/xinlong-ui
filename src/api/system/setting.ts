import { http } from '@/utils/http/axios';
export const saveSetting = (params) => {
  return http.request({
    url: '/sys/setting',
    method: 'POST',
    params,
  });
};
export const getSetting = (params) => {
  return http.request({
    url: '/sys/setting/get',
    method: 'GET',
    params,
  });
};
