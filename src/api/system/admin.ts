import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

export interface BasicPageParams {
  pageNumber: number;
  pageSize: number;
  total: number;
}

/**
 * @description: 获取用户信息
 */
export function getAdminInfo() {
  return http.request({
    url: '/sys/admin/info',
    method: 'GET',
  });
}

/**
 * @description: 用户登录
 */
export function login(params) {
  return http.request<BasicResponseModel>(
    {
      url: '/sys/admin/login',
      method: 'GET',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 刷新token
 */
export function refreshToken(params) {
  return http.request({
    url: '/sys/admin/token',
    method: 'GET',
    params,
  });
}

/**
 * @description: 用户修改密码
 */
export function changePassword(params, uid) {
  return http.request(
    {
      url: `/admin/u${uid}/changepw`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 用户登出
 */
export function logout(params) {
  return http.request({
    url: '/login/logout',
    method: 'POST',
    params,
  });
}

/**
 * @description: 用户修改个人资料
 */
export function changeInfo(params) {
  return http.request(
    {
      url: `/sys/admin/setting/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 系统用户列表
 */
export function getAdminPage(params?) {
  return http.request({
    url: '/sys/admin/page',
    method: 'GET',
    params,
  });
}

/**
 * 保存系统用户
 * @param params
 */
export function saveAdmin(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/sys/admin`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 编辑系统用户
 * @param params
 */
export function updateAdmin(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/sys/admin/${params.id}`,
      method: 'PUT',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除系统用户
 * @param params
 */
export function deleteAdmin(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/sys/admin/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
