import { http } from '@/utils/http/axios';

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

/**
 * @description: 根据用户id获取用户菜单
 * 遗留 暂时没用
 */
export function adminMenus() {
  return http.request({
    url: '/menus',
    method: 'GET',
  });
}

/**
 * 获取tree菜单列表
 * @param params
 */
export function getMenuTree(params?) {
  return http.request({
    url: '/dev/menu/tree',
    method: 'GET',
    params,
  });
}

/**
 * 新增保存菜单
 * @param params
 */
export function saveMenu(params?) {
  return http.request<BasicResponseModel>(
    {
      url: '/dev/menu/save',
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 修改保存菜单
 * @param params
 */
export function updateMenu(params?) {
  return http.request<BasicResponseModel>(
    {
      url: '/dev/menu/update',
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * 删除菜单
 * @param params
 */
export function deleteMenu(params?) {
  return http.request<BasicResponseModel>(
    {
      url: `/dev/menu/delete/${params.id}`,
      method: 'DELETE',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}
