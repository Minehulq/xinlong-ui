export const blindBoxColumns = [
  {
    type: 'selection',
  },
  {
    title: 'ID',
    key: 'id',
    width: 35,
  },
  // {
  //   title: '分类',
  //   key: 'classifyName',
  //   width: 70,
  // },
  // {
  //   title: '品牌',
  //   key: 'brandName',
  //   width: 70,
  // },
  {
    title: '商品名称',
    key: 'blindBoxName',
    width: 150,
  },
  // {
  //   title: '商品编号',
  //   key: 'goodsSn',
  //   width: 100,
  // },
  // {
  //   title: '商品简介',
  //   key: 'goodsDesc',
  //   width: 100,
  // },
  {
    title: '价格',
    key: 'price',
    width: 70,
  },
];
